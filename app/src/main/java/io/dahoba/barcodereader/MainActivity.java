package io.dahoba.barcodereader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class MainActivity extends AppCompatActivity {


    Button scanBtn;
    TextView resultText;
    private final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scanBtn = (Button) findViewById(R.id.scanBtn);
        resultText = (TextView) findViewById(R.id.textResult);
        scanBtn.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
//                scanBarCode();
                scanWithStandaloneApp();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                Log.d(TAG, "contents: " + contents);
                resultText.setText(contents);
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "RESULT_CANCELED");
            }
        }
        IntentResult resultData  = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(resultData!=null){
            Log.v(TAG," result: "+resultData);
            Log.v(TAG," content: "+resultData.getContents());
            Log.v(TAG," format: "+resultData.getFormatName());
            resultText.setText(resultData.toString());
        }
    }

    private void scanBarCode() {
        IntentIntegrator mIntegrator = new IntentIntegrator(this);
        mIntegrator.initiateScan();
    }

    private void scanWithStandaloneApp(){
        Intent intent = new Intent(getApplicationContext(),CaptureActivity.class);
        intent.setAction("com.google.zxing.client.android.SCAN");
        intent.putExtra("SAVE_HISTORY", false);
        startActivityForResult(intent, 2);
    }

}
