# Barcode scanner

```xml
<manifest ... >
    <uses-feature android:name="android.hardware.camera"
                  android:required="true" />
...
</manifest>

```

`app/build.gradle`:

```json
dependencies {
...
    compile 'com.google.zxing:android-integration:3.3.0'
    compile 'com.tarun0.zxing-standalone:zxing-standalone:1.0.0'
...
}
```

layout:

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:padding="@dimen/standard_padding"
    tools:context="io.dahoba.barcodereader.MainActivity">

    <Button
        android:id="@+id/scanBtn"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_centerHorizontal="true"
        android:gravity="center"
        android:text="@string/scanBtnText" />

    <TextView
        android:id="@+id/textResult"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_centerHorizontal="true"
        android:text="Hello World!" />

</LinearLayout>
```

## Scan barcode with the barcode scanner app

```java
...
    resultText = (TextView) findViewById(R.id.textResult);
    scanBtn = (Button) findViewById(R.id.scanBtn);
    scanBtn.setOnClickListener(new Button.OnClickListener() {

        @Override
        public void onClick(View view) {
            scanBarCode();
        }
    });
...
private void scanBarCode() {
    IntentIntegrator mIntegrator = new IntentIntegrator(this);
    mIntegrator.initiateScan();
}
...
```

## Get result

```java
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult resultData  = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(resultData!=null){
            Log.v(TAG," result: "+resultData);
            Log.v(TAG," content: "+resultData.getContents());
            Log.v(TAG," format: "+resultData.getFormatName());
            resultText.setText(resultData.toString());
        }
    }
```

# Scan barcode using embedded app

```java
    private void scanWithStandaloneApp(){
        Intent intent = new Intent(getApplicationContext(),CaptureActivity.class);
        intent.setAction("com.google.zxing.client.android.SCAN");
        intent.putExtra("SAVE_HISTORY", false);
        startActivityForResult(intent, 2);
    }

    ...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    if (requestCode == 2) {
        if (resultCode == RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            Log.d(TAG, "contents: " + contents);
            resultText.setText(contents);
        } else if (resultCode == RESULT_CANCELED) {
            Log.d(TAG, "RESULT_CANCELED");
        }
    }
    ...

        IntentResult resultData  = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
    }
```
